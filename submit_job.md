**Tara User Guide**
=====================

**หัวข้อ**
---------
- [เกี่ยวกับระบบ TARA (Overview of TARA)](Tara_User_Guide.md)
- [การลงทะเบียนและเข้าใช้งานระบบ TARA (Registration and Login)](access.md)
- [พื้นที่ใช้งานและการโอนข้อมูล (File Storage and transfer)](file_storage_transfer.md)
- [การใช้งาน Application (How to Use Application System)](application.md)
- [การรัน Source code (Compiling Source Code)](compile.md)
- [SLURM คืออะไร (Introduction to SLURM)](introduction_slurm.md)
- **การสั่งรัน Job ใน SLURM โดยใช้ Script (Running Jobs by SLURM Script)**
- [การสั่งรัน Job ใน SLURM ในรูปแบบ Interactive (Running Jobs by SLURM Interactive)](interactive_job.md)
- [ระบบแจ้งปัญหาและสอบถามข้อมูล (ThaiSC Support)](support.md)
&nbsp;

**การสั่งรัน Job ใน SLURM โดยใช้ Script (Running Jobs by SLURM Script)**
---------------------------------------------------------------------

การสั่งรันงาน (Submit job) เพื่อไปเข้าคิวในการรันงาน โดยการ Submit จะใช้ bash script ในการ Submit โดยมีขั้นตอนดังต่อไปนี้

1. เมื่ออยู่ใน `path` ที่ต้องการจะรันงานหรือโค้ดที่รันงาน ให้ทำการสร้าง bash script โดยใช้คำสั่ง `vi sbatch_script.sh` หรือ `nano sbatch_script.sh`

2. ระบุรายละเอียดที่ต้องการเข้าไปใน bash script ตาม Template ที่มีการเตรียมมาให้ด้านล่าง

```
#!/bin/bash
#SBATCH -p compute			    # ระบุ partition หรือประภทเครื่องที่ใช้งาน [Compute/Memory/GPU]
#SBATCH -N 1 --ntasks-per-node=40	    # ระบุจำนวนเครื่อง (nodes) และ จำนวน core ต่อ node
#SBATCH -t 00:10:00		            # ระบุเวลาที่ต้องการจองหรือใช้งาน (time limit) สูงสุด โดยมีรูปแบบคือ <hh:mm:ss> ชั่วโมง:นาที:วินาที
#SBATCH -J my_job			    # ระบุชื่อของ Job 
#SBATCH –A tutorial			    # ระบุ Project account ซึ่งจะได้หลังจากการ Register **หากไม่ระบุในส่วนนี้จะไม่สามารถรันงานได้

#SBATCH <advance feature>                   # สามารถระบุ option เพิ่มเติมอีกได้

module purge				    #unload module ทั้งหมด เพราะว่าอาจจะมีการ Load module ไว้ก่อนหน้านั้น
module load intel			    #load module ที่ต้องการใช้งาน ตัวอย่างนี้คือ intel

srun  my_hpc_script 		            #สั่งรัน program/executable code ของท่าน
```

&nbsp;

รายละเอียด option เพิ่มเติม (List all for sbatch feature)
-----------------------------------

<!-- markdownlint-enable MD013 -->
|              Sbatch option             |                                                    รายละเอียด                                                    |
|:---------------------------------------|:----------------------------------------------------------------------------------------------------------------|
|`#SBATCH --mail-type=NONE `             | การ Submit job หรือรันงานเสร็จ จะมีการส่ง Email เพื่อแจ้งสถานะ หากไม่ต้องการรับ Email สามารถระบุคำสั่งนี้เพิ่มเติมเข้าไป              |
|`#SBATCH –-begin=16:00`                 | ระบุเวลาสั่ง Submit คือ สั่ง Submit job เวลา 16:00 ของวันนี้ รูปแบบคือ <hh:mm:ss>                                          |
|`#SBATCH –-begin=now+1`                 | ระบุเวลาสั่ง Submit คือ สั่ง Submit job หลังจากนี้ไปอีก 1 ชั่วโมง                                                           |
|`#SBATCH –-begin=2019-02-14T12:00:00`   | ระบุวันเวลาเพื่อสั่ง Submit Start  T=เวลา โดยมีรูปแบบคือ <yyyy-mm-ddThh:mm:ss>                                            |
|`#SBATCH –-cpu-per-task=2`              | ระบุทรัพยากร CPU ต่อ task                                                                                          |
|`#SBATCH –-dependency=afterok:<jobID>`  | ระบุการ Submit job หลังจาก jobID ที่ระบุรันงานเสร็จสิ้น                                                                   |
|`SBATCH –-exclusive`                    | ระบุให้ Node ที่สั่งรันหรือจอง ไม่สามารถทำการแบ่งให้ผู้ใช้งานอื่น ร่วมใช้งานได้                                                     |
|`#SBATCH –-mem=16`                      | ระบุ memory เพื่อใช้งานใน node (ค่าปกติคือ gbytes) หรือระบุ **--mem=MaxMemPerNode** เพื่อใช้ memory ทั้งหมดภายใน node นั้น      |
|`#SBATCH –-mem-per-cpu=16`              | ระบุ memory ใช้งานต่อ cpu (ค่าปกติคือ gbytes) หรือระบุ **--mem=MaxMemPerNode** เพื่อใช้ memory ทั้งหมดเพื่อใช้งานต่อ CPU นั้น      |
|`#SBATCH -–output=slurm-%A.out`         | ระบุชื่อไฟล์ของผลที่ได้หลังจากรันเสร็จสิ้น โดยปกติจะมาในรูปแบบชื่อ JobID .out สามารถระบุนามสกุลไฟล์ได้ , "%A" คือใส่ค่า jobID ในชื่อไฟล์|
|`#SBATCH –-test-only`                   | ทดสอบการ submit job ว่าผ่านหรือไม่ ปล.ไม่ใช่การ Submit จริง                                                            |
<!-- markdownlint-enable MD013 -->

&nbsp;

ตัวอย่างการใส่ option เพิ่มเติม
-------------------------
```
#!/bin/bash
#SBATCH -p compute			    # ระบุ partition หรือประภทเครื่องที่ใช้งาน
#SBATCH -N 1 --ntasks-per-node=40	    # ระบุจำนวนเครื่อง (nodes) และ จำนวน core ต่อ node
#SBATCH -t 00:10:00		            # ระบุเวลาที่ต้องการจองหรือใช้งาน (time limit) โดยมีรูปแบบคือ <hh:mm:ss> ชั่วโมง:นาที:วินาที
#SBATCH -J my_job			    # ระบุชื่อของ Job 
#SBATCH –A tutorial			    # ระบุ Project account ซึ่งจะได้หลังจากการ Register **หากไม่ระบุในส่วนนี้จะไม่สามารถรันงานได้

#SBATCH --mail-type=NONE                    # ไม่ให้ส่ง Email log
#SBATCH –-mem=16                            # ระบุใช้ memory 16Gb

module purge				    #unload module ทั้งหมด เพราะว่าอาจจะมีการ Load module ไว้ก่อนหน้านั้น
module load foss			    #load module ที่ต้องการใช้งาน ตัวอย่างนี้คือ Foss

srun  my_hpc_script 		            #สั่งรัน program/executable code ของท่าน
```

***Tips ntasks-per-node สูงสุดคือ CPU core per node= 40 ( tara-c-[001-060] ) and CPU core per node = 192 ( tara-m-[001-010] )***

&nbsp;

3. เมื่อทำการระบุค่าต่างๆ ภายใน bash script เสร็จเรียบร้อยให้ทำการบันทึก 

4. เมื่อบันทึกเสร็จเรียบร้อย ใช้คำสั่ง `sbatch` ตามด้วยชื่อ bash script ไฟล์ของท่านเพื่อทำการ Submit job เข้าสู่ SLURM ดังดัวอย่าง

```
sbatch sbatch_script.sh
```

เท่านี้การ Submit job เสร็จสิ้น โปรแกรมของท่านก็จะไปเข้าคิวเพื่อรอรันงาน

&nbsp;

ตัวอย่างการรันโปรแกรมเฉพาะทาง (Running Specific applications)
---------------------------------------------------------------
ในหัวข้อนี้พูดถึงการทำงานโดยใช้โปรแกรมเฉพาะทางในที่นี้คือ Gaussian 16 และ Python 

### Gaussian
Gaussian เป็นแพคเกจซอฟต์แวร์ทางเคมีเชิงคำนวณสำหรับคำนวณโครงสร้างทางเคมี โดยมีวิธีส่งงาน Gaussian ในระบบ TARA HPC ดังนี้

1. เตรียมไฟล์ input ของ Gaussian ด้วยชื่อไฟล์ที่เหมาะสม ในที่นี่ชื่อ myjob.com 
**หมายเหตุ: ต้องมีบรรทัดว่างเปล่าสุดท้าย:

```
//Example Gaussian input for geometry optimization of H2O 

%mem=10GB
%nprocshared=40
%chk=h20.chk
# opt HF/6-31G(d)	
 
water energy	
 
0   1	
O  -0.464   0.177   0.0	 
H  -0.464   1.137   0.0	 
H   0.441  -0.143   0.0


```

&nbsp;

2. สร้างไฟล์สำหรับการ Submit job เข้าสู่ระบบ TARA โดยเตรียม Script สำหรับการ Submit job โดยชื่อ submitg16.sub (ใน Job นี้ ใช้งาน 1 compute node ด้วย 40 processor cores)

```
#!/bin/bash -l
######## select compute node ########
#SBATCH -p compute                      	#if you select compute node
#SBATCH -N 1 --ntasks-per-node=40       	#specific number of nodes and task per node
#SBATCH -t 60:00:00                         #job time limit <hr:min:sec>
#SBATCH -J job_name                         #job name
#SBATCH –A tutorial			                # ระบุ Project account ซึ่งจะได้หลังจากการ Register **หากไม่ระบุในส่วนนี้จะไม่

module purge                            	#purge all module
module load intel                         	#load intel module for MPI run

FILENAME=job_name
WORKDIR=$SLURM_SUBMIT_DIR
######################################################

cd $WORKDIR

###### Identify Gaussian root ##############################
export g16root=/tarafs/.../Gaussian                          #เรียกโปรแกรม Gaussian
export GAUSS_SCRDIR=/tarafs/.../g16/$USER/$SLURM_JOB_ID
source $g16root/g16/bsd/g16.profile

rm -rf   /tarafs/.../g16/$USER/$SLURM_JOB_ID
mkdir -p /tarafs/.../g16/$USER/$SLURM_JOB_ID

cp $FILENAME.gjf $GAUSS_SCRDIR
cp $FILENAME.chk $GAUSS_SCRDIR
cd $GAUSS_SCRDIR

##run_GAUSSIAN  $FILENAME.com

g16 < $FILENAME.com >> $WORKDIR/$FILENAME.log.$SLURM_JOB_ID

cp *.chk $WORKDIR/$FILENAME.chk
cp $FILENAME.fch* $WORKDIR/
cp *.cu* $WORKDIR/
cd $WORKDIR
mv $FILENAME.log.$SLURM_JOB_ID $FILENAME.DONE.log

# Clean up scratch space

rm -rf $GAUSS_SCRDIR/

#----- End of g16 SLURMJOB ---------
```
สามารถดูตัวอย่าง Option เพิ่มเติมของการ Submit job ได้ที่ [การสั่งรัน Job ใน SLURM (Running Jobs in SLURM)](submit_job.md) 

&nbsp;

3. Submit the job
```
$ sbatch submitg16.sub      #Submit job

$ squeue -t your_username   #Check queue job
```

สามารถดูผลลัพธ์ของการรัน Gaussian ได้ที่ myjob.log โดยจะอยู่ที่โฟลเดอร์เดียวกับไฟล์ Submit job

&nbsp;

### Python
ตัวอย่าง Python code
1. ทำการ vi สร้างไฟล์โปรแกรม Python ในที่นี้ชื่อ myjob.py โดยมีรายละเอียดดังนี้
```
print ("Hello, world!")
```

2. สร้างไฟล์สำหรับการ Submit job เข้าสู่ระบบ TARA โดยเตรียม Script สำหรับการ Submit job โดยชื่อ submitpython.sub
```
#!/bin/bash -l
#SBATCH -p compute                                #specific partition (compute, memory, gpu)
#SBATCH -N 1 --ntasks-per-node=1                  #specific number of nodes and task per node
#SBATCH -t 1:00:00                     	          #job time limit <hr:min:sec>
#SBATCH -J job_name                               #job name
##SBATCH –A tutorial			                  # ระบุ Project account ซึ่งจะได้หลังจากการ Register **หากไม่ระบุในส่วนนี้จะไม่

module purge                            	      #purge all module
module load Python                                #load python module 

python myjob.py

```

3. Submit the job
```
$ sbatch submitg16.sub      #Submit job

$ squeue -t your_username   #Check queue job
```

สามารถดูผลลัพธ์ของการรันที่ไฟล์ <jobid>.out ผลที่ได้คือ

```
Hello, world!
```

#### สามารถดูรายละเอียดเพิ่มเติมของ sbatch ได้ที่ **https://slurm.schedmd.com/sbatch.html**

&nbsp;

การยกเลิกการรันงาน (Cancelling jobs)
----------------------------------

สามารถยกเลิกการรันงานโดยใช้คำสั่ง `scancel` ตามด้วย JobID ที่ต้องการยกเลิก
```
$ scancel [JOBID]
```

 ตัวอย่างเช่น

```
 `$ scancel 1234`
 ```

 การดูสถานะของ Job (Show job status)
-----------------------------------

`$ squeue`  คือ คำสั่งที่ใช้แสดงสถานะของ Job 

```
[tara@tara-frontend-1-node-ib ~]$ squeue
JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
2138   compute FeN3G_T2 snamungr PD       0:00      1 (Resources)
1866   compute diamond_ schinkan  R   11:06:07      1 tara-c-009
2040   compute diamond_ schinkan  R      26:51      3 tara-c-[002,033,057]
2039   compute diamond_ schinkan  R    2:16:35      1 tara-c-023
```
โดยมีรายละเอียดดังนี้
- JobID คือ รหัสของ Job
- Partition บอกว่า Job นี้ทำการรันอยู่ที่ Partition กลุ่มไหน
- Name คือ ชื่อของ Job ที่ทำการระบุไว้ในการ Submit job
- User คือ ผู้ใช้งานที่ทำการรัน Job
- ST คือ Status การทำงานของ Job นั้นๆ ยกตัวอย่างเช่น PD = Pending (รอคิว), R = Running (กำลังรันอยู่) เป็นต้น
- Time คือ เวลาที่ใช้รันงานของ Job
- Nodes คือ Job นี้ใช้ จำนวนกี่เครื่องในการรัน
- Node list คือ รายชื่อเครื่องที่ Job นี้ ใช้ในการรัน ได้แก่ Compute node(tara-c-xxx), Memory node(tara-m-xxx), GPU node(tara-g-xxx) 

&nbsp;

### การดูสถานะของ Job โดยระบุชื่อผู้ใช้งาน (User)

`$ squeue -u [User]` 	ใช้คำสั่ง `$ squeue` โดยระบุ `-u [User]` เพิ่มเข้าไปโดยใส่ค่าตรง [User] เป็น Userที่ต้องการดู
```
[tara@tara-frontend-1-node-ib ~]$ squeue -u tara
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
             17986   compute Dimer_N2     tara  R 4-10:08:27      2 tara-c-[046,057]
             18082   compute N4-top-t     tara  R 4-01:05:51      2 tara-c-[036,059]
             20178   compute       sv     tara  R       0:40      1 tara-c-010
```

&nbsp;

### การดูสถานะของ Job โดยระบุ Partition

`$ squeue -p [partition]`
```
[tara@tara-frontend-1-node-ib ~]$ squeue -p gpu
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
             18267       gpu  voc_ssd     tara  R 2-00:13:12      1 tara-g-001
```

&nbsp;

### การดูสถานะของ Job โดยระบุสถานะต่างๆ ของ Jobs

`$ squeue -t PD`	ระบุค่าเป็น `-t PD` เพื่อแสดง Jobs ที่มีสถานะรอคอย (Pending Jobs)
```
[tara@tara-frontend-1-node-ib ~]$ squeue -t PD
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
             20149    memory svaba_As     tara PD       0:00      1 (Priority)
             20150    memory svaba_As     tara PD       0:00      1 (Priority)
             20151    memory svaba_As     tara PD       0:00      1 (Priority)
             20152    memory svaba_As     tara PD       0:00      1 (Priority)
             20134    memory svaba_As     tara PD       0:00      1 (Resources)
             20153    memory svaba_As     tara PD       0:00      1 (Priority)
             20119   compute Hy_SiOBr     tara PD       0:00      1 (Resources)
             20120   compute Hy_SiOBr     tara PD       0:00      1 (Priority)
             20121   compute Hy_SiOH1     tara PD       0:00      1 (Priority)
```
`$ squeue -t R`	ระบุค่าเป็น `-t R` เพื่อแสดง Jobs ที่มีสถานะรัน (Running Jobs)
```
[tara@tara-frontend-1-node-ib ~]$ squeue -t R
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
             19875    memory  16World     tara  R   19:00:11      1 tara-m-002
             19869    memory  22World     tara  R   19:01:18      1 tara-m-002
             19969    memory   16Mcep     tara  R   18:11:40      1 tara-m-002
             19971    memory   44Mcep     tara  R   18:10:14      1 tara-m-006
             19970    memory   22Mcep     tara  R   18:11:00      1 tara-m-006
             19978    memory  sat-1ht     tara  R   15:39:59      1 tara-m-010
             20167    memory cmd_kour     tara  R      44:24      1 tara-m-004
             19991    memory  sat-5ht     tara  R    5:55:27      2 tara-m-[002,004]
             18267       gpu  voc_ssd     tara  R 2-00:31:00      1 tara-g-001
             18679   compute rev10Ef_     tara  R 1-04:39:55      1 tara-c-039
             18678   compute rev20_Te     tara  R 1-04:41:04      1 tara-c-039
             18677   compute rev25_Te     tara  R 1-04:42:24      1 tara-c-039
```

`$ squeue -t F`	ระบุค่าเป็น `-t F` เพื่อแสดง Jobs ที่มีสถานะผิดพลาด (Fail Jobs)

เราสามารถระบุ Filter สองอย่างเข้าไปด้วยกันได้ เช่น `$ squeue -t R -u tara` คือ การดูสถานะของ Jobs ของ Tara ที่ทำการรันอยู่ เป็นต้น

`$ scontrol show job [JOBID]`  ดูรายละเอียดต่างๆ ของ Job เช่น ดูว่า Job ใช้ CPU, Memory เท่าไหร่, ใช้เวลาไปเท่าไหร่, เวลาที่ระบบ TARA จะเริ่มรัน (StartTime) และ เวลาที่คาดว่าจะรันเสร็จ (EndTime) ซึ่งจะทำให้สามารถบริหารการจองทรัพยากรครั้งต่อไปได้ดียิ่งขึ้น
```
Submitted batch job 36309
[tara@tara-frontend-1-node-ib ~]$ scontrol show job 36309
JobId=36309 JobName=test_sbatch
   UserId=tara(1997000023) GroupId=tara(1997000023) MCS_label=N/A
   Priority=58753 Nice=0 Account=thaisc QOS=thaisc
   JobState=RUNNING Reason=None Dependency=(null)
   Requeue=0 Restarts=0 BatchFlag=1 Reboot=0 ExitCode=0:0
   RunTime=INVALID TimeLimit=00:10:00 TimeMin=N/A
   SubmitTime=2019-06-20T11:22:51 EligibleTime=2019-06-20T11:22:51
   AccrueTime=2019-06-20T11:22:51
   StartTime=2019-06-20T11:22:52 EndTime=2019-06-20T11:32:52 Deadline=N/A
   PreemptTime=None SuspendTime=None SecsPreSuspend=0
   LastSchedEval=2019-06-20T11:22:52
   Partition=compute AllocNode:Sid=tara-frontend-1-node-ib:30010
   ReqNodeList=(null) ExcNodeList=(null)
   NodeList=tara-c-037
   BatchHost=tara-c-037
   NumNodes=1 NumCPUs=1 NumTasks=1 CPUs/Task=1 ReqB:S:C:T=0:0:*:*
   TRES=cpu=1,mem=4800M,node=1,billing=1
   Socks/Node=* NtasksPerN:B:S:C=0:0:*:* CoreSpec=*
   MinCPUsNode=1 MinMemoryCPU=4800M MinTmpDiskNode=0
   Features=(null) DelayBoot=00:00:00
   Reservation=root_11
   OverSubscribe=OK Contiguous=0 Licenses=(null) Network=(null)
   Command=/.../sbatch.sh
   WorkDir=/.../tara
   StdErr=/.../slurm-36309.out
   StdIn=/dev/null
   StdOut=/.../slurm-36309.out
   Power=
```
