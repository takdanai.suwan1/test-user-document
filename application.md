**Tara User Guide**
=======================

**หัวข้อ**
-------
- [เกี่ยวกับระบบ TARA (Overview of TARA)](Tara_User_Guide.md)
- [การลงทะเบียนและเข้าใช้งานระบบ TARA (Registration and Login)](access.md)
- [พื้นที่ใช้งานและการโอนข้อมูล (File Storage and Transfer)](file_storage_transfer.md)
- **การใช้งาน Application (How to Use Application System)**
- [การรัน Source code (Compiling Source Code)](compile.md)
- [SLURM คืออะไร (Introduction to SLURM)](introduction_slurm.md)
- [การสั่งรัน Job ใน SLURM โดยใช้ Script (Running Jobs by SLURM Script)](submit_job.md)
- [การสั่งรัน Job ใน SLURM ในรูปแบบ Interactive (Running Jobs by SLURM Interactive)](interactive_job.md)
- [ระบบแจ้งปัญหาและสอบถามข้อมูล (ThaiSC Support)](support.md)

&nbsp;

**Module and Local Module**
----------------------------
### Module management
Module คือ Framework, Software หรือ Library บนระบบคอมพิวเตอร์ประสิทธิภาพสูง (HPC) โดย Module ช่วยให้ผู้ใช้งานสามารถใช้งานหรือทำงานบนระบบคอมพิวเตอร์ประสิทธิภาพสูง (HPC) ได้อย่างมีประสิทธิภาพ โดยจะแบ่งเนื้อหาเป็น 2 ส่วน ได้แก่

- Environment module system (Lmod)
- EasyBuild

&nbsp;

Environment module system (Lmod)
---------------------------------
  ระบบจัดการ Module คือ ระบบ Software กลางที่ช่วยให้การเรียกใช้งานหรือจัดการ Module ทำได้ง่ายและสะดวกมากยิ่งขึ้น โดยที่ทางผู้ใช้งานไม่ต้องทำการติดตั้ง Software ใช้งานด้วยตัวเอง

  แบ่งเป็นหัวข้อย่อยๆ ดังนี้
- การแสดงรายชื่อ module ที่ให้บริการ (List all available module)
- การดูรายละเอียดของ module (View Description of module)
- การแสดง module ที่ทำการ load (List loaded module)
- การ load module (Load a module)
- การยกเลิก load modules ทั้งหมด(Unload all modules)

&nbsp;

### การแสดงรายชื่อ module ที่ให้บริการ (List all available module)

ใช้คำสั่ง `module avail` หรือ `ml av` เพื่อดูรายชื่อของ Module กลางทั้งหมดที่ทางศูนย์ ThaiSC มีให้บริการ 

```
$ module avail
----------------------------------------------------------- /tarafs/utils/modules/modules/all ------------------------------------------------------------
   Autoconf/2.69-GCCcore-7.3.0               OpenMPI/3.1.1-GCC-7.3.0-2.30                      hwloc/1.11.10-GCCcore-7.3.0
   Autoconf/2.69-GCCcore-8.2.0      (D)      OpenMPI/3.1.3-GCC-8.2.0-2.31.1             (D)    hwloc/1.11.11-GCCcore-8.2.0                          (D)
   Automake/1.16.1-GCCcore-7.3.0             Python/2.7.15-foss-2018b                          icc/2019.1.144-GCC-8.2.0-2.31.1
   Automake/1.16.1-GCCcore-8.2.0    (D)      Python/3.6.6-foss-2018b                    (D)    iccifort/2019.1.144-GCC-8.2.0-2.31.1
   Autotools/20180311-GCCcore-7.3.0          SQLite/3.24.0-GCCcore-7.3.0                       ifort/2019.1.144-GCC-8.2.0-2.31.1
   Autotools/20180311-GCCcore-8.2.0 (D)      ScaLAPACK/2.0.2-gompi-2018b-OpenBLAS-0.3.1        iimpi/2019a
   Bison/3.0.4-GCCcore-7.3.0                 ScaLAPACK/2.0.2-gompi-2019a-OpenBLAS-0.3.5 (D)    imkl/2019.1.144-iimpi-2019a
   Bison/3.0.4                               Tcl/8.6.8-GCCcore-7.3.0                           impi/2018.4.274-iccifort-2019.1.144-GCC-8.2.0-2.31.1
   Bison/3.0.5-GCCcore-7.3.0                 XZ/5.2.4-GCCcore-7.3.0                            intel/2019a
   Bison/3.0.5-GCCcore-8.2.0                 XZ/5.2.4-GCCcore-8.2.0                     (D)    libffi/3.2.1-GCCcore-7.3.0
   Bison/3.0.5                      (D)      binutils/2.30-GCCcore-7.3.0                       libpciaccess/0.14-GCCcore-7.3.0
   CUDA/10.0.130                             binutils/2.30                                     libpciaccess/0.14-GCCcore-8.2.0                      (D)
   EasyBuild/3.8.1                           binutils/2.31.1-GCCcore-8.2.0              (L)    libreadline/7.0-GCCcore-7.3.0
   FFTW/3.3.8-gompi-2018b                    binutils/2.31.1                            (D)    libtool/2.4.6-GCCcore-7.3.0
   FFTW/3.3.8-gompi-2019a           (D)      bzip2/1.0.6-GCCcore-7.3.0                         libtool/2.4.6-GCCcore-8.2.0                          (D)
   GCC/7.3.0-2.30                            flex/2.6.4-GCCcore-7.3.0                          libxml2/2.9.8-GCCcore-7.3.0
   GCC/8.2.0-2.31.1                 (L,D)    flex/2.6.4-GCCcore-8.2.0                          libxml2/2.9.8-GCCcore-8.2.0                          (D)
   GCCcore/7.3.0                             flex/2.6.4                                 (D)    ncurses/6.0
   GCCcore/8.2.0                    (L,D)    foss/2018b                                        ncurses/6.1-GCCcore-7.3.0                            (D)
   GMP/6.1.2-GCCcore-7.3.0                   foss/2019a                                 (D)    numactl/2.0.11-GCCcore-7.3.0
   M4/1.4.17                                 gettext/0.19.8.1                                  numactl/2.0.12-GCCcore-8.2.0                         (D)
   M4/1.4.18-GCCcore-7.3.0                   gompi/2018b                                       xorg-macros/1.19.2-GCCcore-7.3.0
   M4/1.4.18-GCCcore-8.2.0                   gompi/2019a                                (D)    xorg-macros/1.19.2-GCCcore-8.2.0                     (D)
   M4/1.4.18                        (D)      help2man/1.47.4-GCCcore-7.3.0                     zlib/1.2.11-GCCcore-7.3.0
   OpenBLAS/0.3.1-GCC-7.3.0-2.30             help2man/1.47.4                                   zlib/1.2.11-GCCcore-8.2.0
   OpenBLAS/0.3.5-GCC-8.2.0-2.31.1  (D)      help2man/1.47.7-GCCcore-8.2.0              (D)    zlib/1.2.11                                          (D)
```
  ***หมายเหตุ*** `(D)` คิอ Default version ของ module ที่ต้องการ Load ในกรณีที่ระบุแค่ชื่อ ไม่ระบุ version

&nbsp;

หากต้องการกรองดูแค่ Module ที่สนใจ สามารถระบุชื่อ Module ลงไปเพื่อที่จะทำการค้นหาได้ง่ายขึ่น ดังตัวอย่างด้านล่างคือ ค้นหาเฉพาะ openmpi module

 ```
 $ module avail openmpi

---------------------- /tarafs/utils/modules/modules/all -----------------------
   OpenMPI/3.1.1-GCC-7.3.0-2.30    OpenMPI/3.1.3-GCC-8.2.0-2.31.1 (D)
 ```

&nbsp;

### การดูรายละเอียดของ module (View Description of module)

ใช้คำสั่ง `module spider` หรือ `ml spider` กรณีที่อยากทราบรายละเอียดของแต่ละ Module ว่ามีการใช้งานประเภทไหนและประกอบด้วย Software หรือ Library อะไรบ้างภายใน Module

```
$ module spider

-------------------------------------------------------------------------------------------------------------------------------------------------------------
The following is a list of the modules currently available:
-------------------------------------------------------------------------------------------------------------------------------------------------------------

  Autoconf: Autoconf/2.69-GCCcore-7.3.0, Autoconf/2.69-GCCcore-8.2.0
    Autoconf is an extensible package of M4 macros that produce shell scripts to automatically configure software source code packages. These scripts can adapt the
    packages to many kinds of UNIX-like systems without manual user intervention. Autoconf creates a configuration script for a package from a template file that
    lists the operating system features that the package can use, in the form of M4 macro calls.

  Automake: Automake/1.16.1-GCCcore-7.3.0, Automake/1.16.1-GCCcore-8.2.0
    Automake: GNU Standards-compliant Makefile generator

  Autotools: Autotools/20180311-GCCcore-7.3.0, Autotools/20180311-GCCcore-8.2.0
    This bundle collect the standard GNU build tools: Autoconf, Automake and libtool

  Bison: Bison/3.0.4-GCCcore-7.3.0, Bison/3.0.4, Bison/3.0.5-GCCcore-7.3.0, Bison/3.0.5-GCCcore-8.2.0, Bison/3.0.5
    Bison is a general-purpose parser generator that converts an annotated context-free grammar into a deterministic LR or generalized LR (GLR) parser employing
    LALR(1) parser tables.

  CUDA: CUDA/10.0.130
    CUDA (formerly Compute Unified Device Architecture) is a parallel computing platform and programming model created by NVIDIA and implemented by the graphics
    processing units (GPUs) that they produce. CUDA gives developers access to the virtual instruction set and memory of the parallel computational elements in
    CUDA GPUs.

  EasyBuild: EasyBuild/3.8.1
    EasyBuild is a software build and installation framework written in Python that allows you to install software in a structured, repeatable and robust way.

  FFTW: FFTW/3.3.8-gompi-2018b, FFTW/3.3.8-gompi-2019a
    FFTW is a C subroutine library for computing the discrete Fourier transform (DFT) in one or more dimensions, of arbitrary input size, and of both real and
    complex data.
```

&nbsp;

`module spider` สามารถกรองดูแค่ Module ที่เราสนใจได้เช่นเดียวกัน
```
$ ml spider openmpi

------------------------------------------------------------------------------------------------------------------
  OpenMPI:
------------------------------------------------------------------------------------------------------------------
    Description:
      The Open MPI Project is an open source MPI-3 implementation.

     Versions:
        OpenMPI/3.1.1-GCC-7.3.0-2.30
        OpenMPI/3.1.3-GCC-8.2.0-2.31.1

------------------------------------------------------------------------------------------------------------------  For detailed information about a specific "OpenMPI" module (including how to load the modules) use the module's full name.
  For example:

$ module spider OpenMPI/3.1.3-GCC-8.2.0-2.31.1
```

&nbsp;

### การแสดง module ที่ทำการ load (List loaded module)

ใช้คำสั่ง `module list` หรือ `ml` เพื่อดู Module ที่ได้มีการ Load ไว้ ตัวอย่างล่างคือ ยังไม่มีการ Load Module เพราะว่ามีข้อความขึ่น No modules loaded

```
$ module list
No modules loaded
```
หากมีการ load modules จะรายละเอียด ดั่งตัวอย่างที่มีการ load module GCC (GCC คือ C complier เพื่อสามารถอ่านโค้ดและทำงานภาษา C ได้)

```
$ module list

Currently Loaded Modules:
  1) GCCcore/8.2.0   2) binutils/2.31.1-GCCcore-8.2.0   3) GCC/8.2.0-2.31.1
```
&nbsp;

### การ load module (Load a module)

ใช้คำสั่ง `module load` หรือ `ml` ตามด้วยเพื่อดูรายชื่อของ Module ที่ต้องการ load
```
$ module list
No modules loaded
$ module load GCC
$ module list

Currently Loaded Modules:
  1) GCCcore/8.2.0   2) binutils/2.31.1-GCCcore-8.2.0   3) GCC/8.2.0-2.31.1
```

&nbsp;

### การยกเลิก load modules ทั้งหมด(Unload all modules)

ใช้คำสั่ง `module purge` หรือ `ml purge` เพื่อทำการยกเลิก Module ที่ load ทั้งหมด
```
$ module list

Currently Loaded Modules:
  1) GCCcore/8.2.0   2) binutils/2.31.1-GCCcore-8.2.0   3) GCC/8.2.0-2.31.1

$ module purge
$ module list
No modules loaded
```

&nbsp;

EasyBuild
----------

EasyBuild คือ software framework ที่ช่วยในการสร้าง module ใช้งานเอง โดยเรียก module ที่สร้างเองนี้ว่า 'local module'

คำสั่งที่ใช้ในการทำ local module มีดังนี้
 
```
$ mu						# Use local module
$ module load EasyBuild				# Load easybuild
$ eb -S <module>				# Search for recipes for module
$ eb <module> -Dr		                # Dry-run to check before install
$ eb <module> -r			        # Install module
```

&nbsp;

ตัวอย่างการทำ local module ของ GCC

1. ใช้คำสั่ง `mu` เพื่อประกาศว่า ต้องการใช้งาน local module
```
$ mu
```

2. ใช้คำสั่ง `module load Easybuild` เพื่อทำการ load โปรแกรม Easybuild มา เพื่อทำ local module

```
$ module load EasyBuild
```

3. ใช้คำสั่ง `eb -S <module>` เพื่อเทำการค้นหา recipes ของ Software
```
eb -S gcc
```

4. ใช้คำสั่ง `eb  <module> -Dr` เพื่อทำการทดสอบหรือเช็คการตั้งค่าต่างๆ ก่อนทำการติดตั้งว่ามีข้อิดพลาดหรือการ Config อะไรหรือไม่
```
eb GCC-8.2.0-2.31.1.eb -Dr
```

5. ใช้คำสั่ง `eb  <module> -r` เพื่อทำการติดตั้ง module ที่ต้องการ

```
eb GCC-8.2.0-2.31.1.eb -r
```

เท่านี้ก็จะมี local module ของผู้ใช้งาน เพื่อใช้งานเองภายในงานของท่าน