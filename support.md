**Tara User Guide**
=======================

**หัวข้อ**
----------
- [เกี่ยวกับระบบ TARA (Overview of TARA)](Tara_User_Guide.md)
- [การลงทะเบียนและเข้าใช้งานระบบ TARA (Registration and Login)](access.md)
- [พื้นที่ใช้งานและการโอนข้อมูล (File Storage and transfer)](file_storage_transfer.md)
- [การใช้งาน Application (How to Use Application System)](application.md)
- [การรัน Source code (Compiling Source Code)](compile.md)
- [SLURM คืออะไร (Introduction to SLURM)](introduction_slurm.md)
- [การสั่งรัน Job ใน SLURM โดยใช้ Script (Running Jobs by SLURM Script)](submit_job.md)
- [การสั่งรัน Job ใน SLURM ในรูปแบบ Interactive (Running Jobs by SLURM Interactive)](interactive_job.md)
- **ระบบแจ้งปัญหาและสอบถามข้อมูล (ThaiSC Support)**

## **ระบบแจ้งปัญหาและสอบถามข้อมูลเพิ่มเติม (ThaiSC Support)**
---------------------------------------
#### หากมีปัญหาการใช้งานหรือต้องการสอบถามข้อมูลเพิ่มเติม สามารถส่งรายละเอียดดังต่อไปนี้ เพื่อความสะดวกในการดำเนินการแก้ไข

- ชื่อ-นามสกุล เจ้าของปัญหา
- รหัส Project หรือ โครงการ
- username ภายในระบบ TARA
- รายละเอียดของปัญหา
- แนบรูปของปัญหาที่เกิดขึ้น และ Script เพื่อความไวต่อการตรวจสอบ

## โดยส่งมาได้ที่ Email: thaisc@nstda.or.th