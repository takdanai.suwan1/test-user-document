**TARA User Guide**
==================================

**หัวข้อ**
-------
- [เกี่ยวกับระบบ TARA (Overview of TARA)](Tara_User_Guide.md)
- [การลงทะเบียนและเข้าใช้งานระบบ TARA (Registration and Login)](access.md)
- [พื้นที่ใช้งานและการโอนข้อมูล (File Storage and transfer)](file_storage_transfer.md)
- [การใช้งาน Application (How to Use Application System)](application.md)
- **การรัน Source code (Compiling Source Code)**
- [SLURM คืออะไร (Introduction to SLURM)](introduction_slurm.md)
- [การสั่งรัน Job ใน SLURM โดยใช้ Script (Running Jobs by SLURM Script)](submit_job.md)
- [การสั่งรัน Job ใน SLURM ในรูปแบบ Interactive (Running Jobs by SLURM Interactive)](interactive_job.md)
- [ระบบแจ้งปัญหาและสอบถามข้อมูล (ThaiSC Support)](support.md)

**การรัน Source code (Compiling Source Code)**
---------------------------------------------
- การ compiling โปรแกรม เพื่อการใช้งานแบบ serial (Serial Compiling)
- การ compiling โปรแกรม MPI เพื่อการใช้งานแบบ parallel  (Parallel Compiling)


&nbsp;

การ compiling โปรแกรม เพื่อการใช้งานแบบ serial (Serial Compiling)
--------------------------------------------------------------
คือ การรันโปรแกรมแบบปกติบน HPC ในตัวอย่างนี้ เริ่มจากตัวอย่างง่ายๆ โดยใช้ภาษา Fortran code ด้วย Code Hello word

```
Example Fortran code
---------------------
!  Fortran 90
PROGRAM hello
    CHARACTER(30) name           ! Fortran 90
    CALL getenv("HOST",name)     ! get run-host name
    PRINT *, 'Runhost:', name, '   hello, world'
END PROGRAM hello   
```

### Example: Intel compiler for serial mode
ทำการ Load module ของ intel compiler ซึ่งใน module ของ intel นี้ ได้มี compiler ของภาษา fortran มาให้ด้วย โดยใช้คำสั่งตามด้านล่าง

```
$ module load intel
$ ifort <myprogram>.f90 -o <my_program> 

```

### Example:Free and open-source software เช่น Foss compiler for serial mode
อีกรูปแบบคือ การใช้ foss compiler ซึ่งใน module ของ foss ได้มี compiler ของภาษา fortran มาให้เช่นเดียวกัน

```
$ module load foss
$ gfortran <myprogram>.f90 -o <my_program> 

```
&nbsp;

 <!-- markdownlint-enable MD013 --> 
| Language  |          Intel Compiler           |               Foss Compiler             | 
|:----------|:----------------------------------|:----------------------------------------|
|Fortran 90 |$ ifort myprogram.f90 -o myprogram |$ gfortran myprogram.f90 -o myprogram    |
|C          |$ icc myprogram.c -o myprogram     |$ gcc myprogram.c -o myprogram           |
|C++        |$ icpc myprogram.cpp -o myprogram  |$ g++ myprogram.cpp -o myprogram         |
<!-- markdownlint-enable MD013 -->

&nbsp;

การ compiling โปรแกรม MPI เพื่อการใช้งานแบบ parallel  (Parallel Compiling)
-----------------------------------------------------------------------
OpenMPI และ Intel MPI (IMPI) คือ การนำวิธี Message-Passing Interface (MPI) standard มาร่วมเพื่อให้สามารถรันงานแบบ Pararell ได้ แต่ต้องมีการเขียน Code ที่รองรับการทำงานแบบ Pararell ด้วยเช่นกัน โดย Libraries ที่ยกตัวอย่างนี้ เป็นการทำงานร่วมกันของ MPI และ compilers สำหรับภาษา C, C++, and Fortran เพื่อสามารถทำงานบน Cluster ได้

```C
Example MPI C code
---------------------

#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    // Print off a hello world message
    printf("Hello world from processor %s, rank %d out of %d processors\n",
           processor_name, world_rank, world_size);

    // Finalize the MPI environment.
    MPI_Finalize();
}
```

### Example: Intel compiler for parallel mode
ทำการ Load module ของ intel compiler และ  compile C โดยใช้คำสั่งตามด้านล่าง

```
$ module load intel
$ mpiicc <myprogram>.c -o <my_program> 

```

### Example:Free and open-source software เช่น Foss compiler for parallel mode
ทำการ Load module ของ Foss compiler และ  compile C โดยใช้คำสั่งตามด้านล่าง

```
$ module load foss          //foss is package for compiler
$ mpicc <myprogram>.c -o <my_program> 

```
&nbsp;

 <!-- markdownlint-enable MD013 --> 
| Language  |            Intel Compiler            |                 Foss Compiler              | 
|:----------|:-------------------------------------|:-------------------------------------------|
|Fortran 90 |$ mpiifort myprogram.f90 -o myprogram |$ mpifort myprogram.f90 -o myprogram        |
|C          |$ mpiicc myprogram.c -o myprogram     |$ mpicc myprogram.c -o myprogram            |
|C++        |$ mpiicpc myprogram.cpp -o myprogram  |$ mpic++ myprogram.cpp -o myprogram         |
<!-- markdownlint-enable MD013 -->