**TARA User Guide**
==================================

**หัวข้อ**
-------
- [เกี่ยวกับระบบ TARA (Overview of TARA)](Tara_User_Guide.md)
- [การลงทะเบียนและเข้าใช้งานระบบ TARA (Registration and Login)](access.md)
- [พื้นที่ใช้งานและการโอนข้อมูล (File Storage and transfer)](file_storage_transfer.md)
- [การใช้งาน Application (How to Use Application System)](application.md)
- [การรัน Source code (Compiling Source Code)](compile.md)
- [SLURM คืออะไร (Introduction to SLURM)](introduction_slurm.md)
- [การสั่งรัน Job ใน SLURM โดยใช้ Script (Running Jobs by SLURM Script)](submit_job.md)
- **การสั่งรัน Job ใน SLURM ในรูปแบบ Interactive (Running Jobs by SLURM Interactive)**
- [ระบบแจ้งปัญหาและสอบถามข้อมูล (ThaiSC Support)](support.md)

&nbsp;

**การสั่งรัน Job ใน SLURM ในรูปแบบ Interactive (Running Jobs by SLURM Interactive)**
----------------------------------------------------------------------------------

การรันงานแบบ Interactive job
---------------------------
Interactive job คือ การสั่งรัน Job รูปแบบนึง โดยจะเหมาะกับการทดสอบในการรันโปรแกรม ว่าโปรแกรมที่เขียนมาทำงานได้ดีหรือไม่ หรือ งานที่ใช้เวลาในการรันไม่นาน เพราะ Interactive job สามารถรันสูงสุดได้เพียง 2 ชั่วโมงเท่านั้น และการรัน interactive job หากทำการปิด Session จะทำการยกเลิกการรันโดยทันที

1. เริ่มการใช้งาน Interactive job โดยใช้คำสั่ง  `sinteract` ดังตัวอย่าง

    หลังจากคำสั่ง `sinteract` ต้องระบุ Project account และสามารถระบุเวลา, ประเภทเครื่อง, จำนวนเครื่องหรือ memory อื่นๆ เพิ่มเติมเข้าไปได้ โดย Interactive job ใช้งานสูงสุดได้ 2 ชั่วโมง
    ```bash
    $ sinteract -p gpu -N [nodes] --time=00:20:00 -A [name of project] 
    กด Enter
    ```

&nbsp;

2. เมื่อใช้คำสั่ง `sinteract` ระบุรายละเอียดของเครื่องที่ต้องใช้งานด้านบน สามารถใช้คำสั่ง `srun` เพื่อสั่งรันโปรแกรมหรือ Code ของท่าน  

    ```
    $ srun [programs or executable code]			 	 #run program or executable code
    ```

    ตัวอย่างการรัน interactive job เวลา 20 นาที โดยใช้จำนวนเครื่อง = 1 node และ 1 tasks per node
    ```bash
    $ sinteract –A tutorial --time=00:20:00 –N 1 --ntasks-per-node=40
    $ srun my_hpc_script
    ```

&nbsp;

การยกเลิกการรันงาน (Cancelling jobs)
----------------------------------
ทำการกด Ctrl + C หรือ พิมพ์ exit เพื่อทำการ terminate ออกจาก Interactive job mode
- **หมายเหตุ** หากทำการออกจาก Interactive job mode โปรแกรมที่ทำการรันหรือทดสอบอยู่ จะถูกทำการยกเลิกโดยทันที