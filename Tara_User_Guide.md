﻿**TARA User Guide**
==================================

**หัวข้อ**
-------
- **เกี่ยวกับระบบ TARA (Overview of TARA)**
- [การลงทะเบียนและเข้าใช้งานระบบ TARA (Registration and Login)](access.md)
- [พื้นที่ใช้งานและการโอนข้อมูล (File Storage and transfer)](file_storage_transfer.md)
- [การใช้งาน Application (How to Use Application System)](application.md)
- [การรัน Source code (Compiling Source Code)](compile.md)
- [SLURM คืออะไร (Introduction to SLURM)](introduction_slurm.md)
- [การสั่งรัน Job ใน SLURM โดยใช้ Script (Running Jobs by SLURM Script)](submit_job.md)
- [การสั่งรัน Job ใน SLURM ในรูปแบบ Interactive (Running Jobs by SLURM Interactive)](interactive_job.md)
- [ระบบแจ้งปัญหาและสอบถามข้อมูล (ThaiSC Support)](support.md)
&nbsp;

คำศัพท์พื้นฐานภายในระบบ TARA (TARA terminology)
---------------------------------------------
<!-- markdownlint-enable MD013 -->
|    Words    |                                                ความหมาย                                                    |
|:-----------:|:-----------------------------------------------------------------------------------------------------------|
|**Node**     | อุปกรณ์แม่ข่าย (Server) ในระบบ TARA cluster ตัวอย่างเช่น compute node, gpu node,high memory node and dgx node     |
|**Core**     | CPU core ภายใน node                                                                                        |
| **Frontend**| อุปกรณ์แม่ข่าย (Server) สำหรับให้ผู้ใช้งาน Login เข้าใช้ระบบ                                                          |
|**Client**   | เครื่องคอมพิวเตอร์ของผู้ใช้งานที่เข้ามาใช้ระบบ TARA                                                                    |
|**Session**  | เหตุการณ์ ณ ช่วงเวลาที่ Client เชื่อมต่อกับ Frontend                                                               |
|**Job**      | งานที่สั่งผ่านระบบ TARA เช่น ซอฟแวร์, โปรแกรมหรือโค้ดที่สั่งทำงานบนระบบ TARA                                           |
|**Queue**    | งานที่รอเข้าไปทำงานในระบบ TARA โดยมีการบริหารจัดการโดยซอฟแวร์ Scheduler                                            |
|**Quota**    | จำนวนที่แสดงข้อจำกัดการใช้งาน ตัวอย่างเช่น พื้นที่ในการเก็บข้อมูล (disk space)                                            |
|**$HOME**    | Folder ของผู้ใช้งานที่อยู่บนระบบ TARA                                                                             |
<!-- markdownlint-enable MD013 -->

&nbsp;

TARA Architecture
-----------------
![Tara Architecture](Pic/Tara_system_architecture2.jpg)

&nbsp;


&nbsp;

รายละเอียดของอุปกรณ์ภายในระบบ TARA (TARA System Overview)
------------------------------------------------------

<!-- markdownlint-enable MD013 -->
|                                   |      TARA Compute node (tara-c)         |         TARA High memory node (tara-m)     |          TARA GPU node (tara-g)        |      TARA DGX node (tara-dgx)    |
|-----------------------------------|:----------------------------------------|:-------------------------------------------|:---------------------------------------|:----------------------|
| Model:                            | Intel(R) Xeon(R) Gold 6148 CPU @ 2.40GHz|Intel(R) Xeon(R) Platinum 8160 CPU @ 2.10GHz|Intel(R) Xeon(R) Gold 6148 CPU @ 2.40GHz| Intel Xeon E5-2698 v4 2.20 GHz    |
|Number of Nodes:               | 60                                       | 10                                          | 2                                      | 1                                 |
|Number of socket(s):               | 2                                       | 8                                          | 2                                      | 2                                 |
|Cores per socket(s):               | 20                                      | 24                                         | 20                                     | 20                                |
|Total cores per TARA Compute node: | 2 x 20 = 40                             | 8 x 24 = 192                               | 2 x 20 = 40                            | 2 x 20 = 40                     |
|Hardware threads per core:         | 1                                       | 1                                          | 1                                      | 2                                 |
|Hardware threads per node:         | 40 x 1 = 40                             | 192 x 1 = 192                              | 40 x 1 = 40                            | 40 x 2 = 80                     |
|RAM:                               | 192G DIMM DDR4                          | 3TB DIMM DDR4 2666 MHz                     | 384G DIMM DDR4                         | 512G DIMM DDR4 2133MHz           |
|Cache:                             | 27.5 MB L3 cache                        | 33 MB L3 cache                             | 27.5 MB L3 cache                       | 50 MB Smart cache            |
|GPU                                | -                                       | -                                          | 2x Nvidia Tesla V100 for PCIe          | 8x Nvidia Tesla V100 for NVLink  |
<!-- markdownlint-enable MD013 -->

&nbsp;

TARA Network
---------

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mellanox’s Infiniband EDR 100 Gbps + low latency interconnect

&nbsp;

TARA Storage
------------
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; IBM Spectrum Scale: 750 TB Fast SSD disk for scratch  space + High capacity SAS data