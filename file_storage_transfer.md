**TARA User Guide**
==================================

**หัวข้อ**
-------
- [เกี่ยวกับระบบ TARA (Overview of TARA)](Tara_User_Guide.md)
- [การลงทะเบียนและเข้าใช้งานระบบ TARA (Registration and Login)](access.md)
- **พื้นที่ใช้งานและการโอนข้อมูล (File Storage and transfer)**
- [การใช้งาน Application (How to Use Application System)](application.md)
- [การรัน Source code (Compiling Source Code)](compile.md)
- [SLURM คืออะไร (Introduction to SLURM)](introduction_slurm.md)
- [การสั่งรัน Job ใน SLURM โดยใช้ Script (Running Jobs by SLURM Script)](submit_job.md)
- [การสั่งรัน Job ใน SLURM ในรูปแบบ Interactive (Running Jobs by SLURM Interactive)](interactive_job.md)
- [ระบบแจ้งปัญหาและสอบถามข้อมูล (ThaiSC Support)](support.md)

TARA Storage
------------
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; IBM Spectrum Scale: 750 TB Fast SSD disk for scratch  space + High capacity SAS data

&nbsp;

TARA File System
-----------
 <!-- markdownlint-enable MD013 --> 
| File system  |    พื้นที่ใช้งาน (Quota)    |                  รายละเอียด              | 
|:------------:|:-----------------------:|:----------------------------------------|
|$HOME         |  10 GB                  | พื้นที่เก็บข้อมูลส่วนบุคคลของผู้ใช้งาน ใช้สำหรับเก็บข้อมูลหรือโค้ด ทดสอบโปรแกรม หรือเก็บผลที่ไม่ใหญ่มาก เป็นต้น ซึ่งในส่วนของ HOME จะมีขนาดให้ตามที่ระบุเท่านั้น จะไม่สามารถขอพื้นที่เพิ่มได้ |
|$scratch    |    -                     | อยู่ระหว่างการหารือถึงนโยบายการใช้งาน                                        |
|$project      |   50 GB                 | พื้นที่หลักสำหรับใช้ทำงาน การรันโปรแกรม การเก็บข้อมูลหรือผลลัพธ์ที่ได้จากการทำงาน เป็นต้น ซึ่งพื้นที่ส่วนนี้ไม่พอใช้งาน และจำเป็นต้องใช้พื้นที่เพิ่มจริงๆ สามารถร้องขอเพิ่มพื้นที่เพิ่มเติมได้ ซึ่งทางทีมผู้ดูแลจะพิจารณาตามความเหมาะสม                                         |
<!-- markdownlint-enable MD013 -->

 - การโอนข้อมูล (File transfer)
    - ระบบปฏิบัติการณ์ Microsoft Windows + MobaXterm
        - *Step 1* Click menu `Session` > menu `SFTP`

            ![mobaftp01](Pic/MobaXterm/create-session.JPG)

            &nbsp;

        - *Step 2* ใส่ค่าดังต่อไปนี้ Remote host = `'tara.nstda.or.th'`,  username = `'Your username'`, Port `22` > Click `OK`

            ![mobaftp02](Pic/MobaXterm/create-ftp.JPG)

            &nbsp;

        - *Step 3* Click `Yes` > Enter `'Your password'`

        - *Step 4* เลือกและลากไฟล์ที่ต้องการมาวางเพื่อทำการโอนข้อมูล



    - ระบบปฏิบัติการ Mac OSX / Linux

        โดยจะใช้ คำสั่ง **scp** (เป็นคำสั่งใน Terminal)
        - *Step 1* เปิดโปรแกรม terminal

        - *Step 2* Upload ข้อมูลจากเครื่องของท่านไปยังระบบ TARA ใช้คำสั่ง `scp -r file.txt yourusername@tara.nstda.or.th` 
        
        - *Step 3* Download ข้อมูลจากเครื่องของท่านไปยังระบบ TARA ใช้คำสั่ง `scp yourusername@tara.nstda.or.th:~/path-on-server/yourfile.txt path-on-labtop/yourfile.txt`

&nbsp;

### การดูขนาดพื้นที่เก็บข้อมูลที่มีอยู่ (Check quota)

สามารถดูขนาดพื้นที่ที่ใช้ไปและคงเหลือ โดยใช้คำสั่ง

`$ check_quota`
```
[tara@tara-frontend-1-node-ib ~]$ check_quota
Home Directory
        $HOME : quota 10G, used 3.52G
Project Directory
```