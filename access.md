**Tara User Guide**
==================================

**หัวข้อ**
-------
- [เกี่ยวกับระบบ TARA (Overview of TARA)](Tara_User_Guide.md)
- **การลงทะเบียนและเข้าใช้งานระบบ TARA (Registration and Login)**
- [พื้นที่ใช้งานและการโอนข้อมูล (File Storage and transfer)](file_storage_transfer.md)
- [การใช้งาน Application (How to Use Application System)](application.md)
- [การรัน Source code (Compiling Source Code)](compile.md)
- [SLURM คืออะไร (Introduction to SLURM)](introduction_slurm.md)
- [การสั่งรัน Job ใน SLURM โดยใช้ Script (Running Jobs by SLURM Script)](submit_job.md)
- [การสั่งรัน Job ใน SLURM ในรูปแบบ Interactive (Running Jobs by SLURM Interactive)](interactive_job.md)
- [ระบบแจ้งปัญหาและสอบถามข้อมูล (ThaiSC Support)](support.md)

การลงทะเบียนเข้าใช้งานระบบ TARA (Register)
--------------------------------------

![TARA Register](Pic/Register.png)

ขั้นตอนการลงทะเบียนเข้าใช้งานระบบ TARA มีดังนี้

1. ทำการกรอกข้อมูลขอเข้าใช้งานที่ >>  **[ฟอร์มลงทะเบียนขอเข้าใช้งานระบบ TARA HPC](https://forms.gle/8vZmJYSgNmFcYk7Q8)**
2. เมื่อทำการกรอกและส่งข้อมูลเรียบร้อย ทางทีม ThaiSC จะสร้างข้อมูลต่างๆ เพื่อส่งกลับให้ผู้ลงทะเบียน ดังนี้
    - เลขโครงการ (ThaiSC Prohect ID)
    - จำนวนทรัพยากรที่ได้รับอนุมัติให้เข้าใช้งานระบบ TARA
    - ระยะเวลาที่ได้รับการอนุมัติเข้าใช้งานระบบ TARA 
    - แจ้งให้เจ้าของโครงการ (PI) และผู้ร่วมโครงการ กรอกแบบฟอร์มการขอเข้าร้วมโครงการวิจัยและพัฒนาของ Team memver บนระบบ TARA HPC
3. กรอกข้อมูลของเจ้าของโครงการ (PI) และผู้ร่วมโครงการ >> **[ฟอร์มลงทะเบียนขอเข้าร่วมโครงการวิจัยและพัฒนาของ Team member บน TARA HPC](https://forms.gle/6CmWALZhDuSWwqpQ7)**
    #### **หมายเหตุ หากไม่กรอกข้อมูลในส่วนนี้ จำทำให้ไม่สามารถอนุมัติโครงการและสร้าง Account ของสมาชิกภายในโครงการได้ ดังนั้นรบกวนกรุณากรอกข้อมูลให้ถูกต้องและครบถ้วน
4. เมื่อท่านกรอกส่งข้อมูลของ ฟอร์มลงทะเบียนขอเข้าร่วมโครงการวิจัยและพัฒนาของ Team member บน TARA HPC เสร็จเรียบร้อย ทางทีมงานจะทำการสร้างและแจ้งข้อมูลกลับ ดังนี้
    - แจ้ง User account ให้กับผู้ร่วมโครงการที่ดำเนินการกรอกข้อมูลใน ฟอร์มลงทะเบียนขอเข้าร่วมโครงการวิจัยและพัฒนาของ Team member เรียบร้อย
    - สร้าง Project accounting ในระบบ TARA HPC เพื่อทำให้สามารถรันงานบนได้
    - สร้าง Project directory และกำหนดสิทธิ์การเข้าถึงให้เฉพาะ PI และผู้ร่วมโครงการที่เข้าเงื่อนไขฯ

&nbsp;

การเข้าใช้งาน (Logging in)
------------------------------------------------
 1. ระบบปฏิบัติการ Microsoft Windows 
    - การเข้าใช้งาน โดยจะใช้ Software MobaXterm สามารถดาวน์โหลดได้ที่ >> **[MobaXterm](https://mobaxterm.mobatek.net/download-home-edition.html)** 
        - เมื่อทำการ Download และติดตั้งเสร็จเรียบร้อย จากนั้นเปิดโปรแกรม MobaXterm

        - *Step 1* กดที่ menu `Session` > menu `SSH` 

            ![moba01](Pic/MobaXterm/create-session.JPG)

            &nbsp;

            ![moba02](Pic/MobaXterm/create-session2.JPG)

            &nbsp;

        - *Step 2* ใส่ค่าดังต่อไปนี้ Remote host = `'tara.nstda.or.th'`,  username = `'Your username'`, Port `22` > กด `OK`

            ![moba03](Pic/MobaXterm/create-session3.JPG) 

            &nbsp;

        - *Step 3* กด `Yes`

            ![moba04](Pic/MobaXterm/create-session4.JPG)

            &nbsp;



2. ระบบปฏิบัติการ Mac OSX / Linux

    - การเข้าใช้งาน โดยจะใช้ คำสั่ง **ssh** (เป็นคำสั่งใน Terminal)
        - *Step 1* เปิดโปรแกรม terminal

        - *Step 2* ใช้คำสั่ง `ssh yourusername@tara.nstda.or.th`

        - *Step 3* ใส่รหัสผ่านของท่าน

    