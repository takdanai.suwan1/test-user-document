**Tara User Guide**
========================

**หัวข้อ**
-------
- [เกี่ยวกับระบบ TARA (Overview of TARA)](Tara_User_Guide.md)
- [การลงทะเบียนและเข้าใช้งานระบบ TARA (Registration and Login)](access.md)
- [พื้นที่ใช้งานและการโอนข้อมูล (File Storage and transfer)](file_storage_transfer.md)
- [การใช้งาน Application (How to Use Application System)](application.md)
- [การรัน Source code (Compiling Source Code)](compile.md)
- **SLURM คืออะไร (Introduction to SLURM)**
- [การสั่งรัน Job ใน SLURM โดยใช้ Script (Running Jobs by SLURM Script)](submit_job.md)
- [การสั่งรัน Job ใน SLURM ในรูปแบบ Interactive (Running Jobs by SLURM Interactive)](interactive_job.md)
- [ระบบแจ้งปัญหาและสอบถามข้อมูล (ThaiSC Support)](support.md)


**SLURM คืออะไร??**
--------------------------------------------------------
### SLURM คือ Scheduler ของระบบ TARA โดยทำหน้าที่ ดังต่อไปนี้
1. บริหารจัดการทรัพยากร (Allocate resources) และ จัดการระบบคิวขณะรอรันงาน (Managing a queue of pending work)
2. มี Framework สำหรับสั่ง starting, executing and monitoring work เพื่ออำนวยความสะดวกในการสั่งงานผ่าน SLURM


&nbsp;

การบริหารจัดการทรัพยากร (Allocate resources)
-----------------------------------------
### ทรัพยากรที่ผู้ใช้งานสามารถกำหนดได้ด้วยตนเอง ยกตัวอย่างเช่น
- จำนวนของเครื่องหรือ cores (Number of nodes/cores)
- เวลาที่ต้องการใช้งาน (Amount of time)
- ประเภทของเครื่องที่่ใช้งาน (Type of machine, Partition)
- จำนวนหน่วยความจำ (Memory)

&nbsp;

PARTITION & QUEUE
------------------------
 อธิบายอย่างง่ายๆ  Partition เปรียบเสมือนป้ายรถโดยสาร โดยที่มีรถโดยสารตสายต่างๆ ให้บริการ ในส่วนของ Queue คือ ประชาชนหรือบุคคลที่มาเข้าคิวรอขึ้นรถโดยสารสายต่างๆ ที่ป้ายรถโดยสาร โดยคำว่า Partition ในระบบ TARA หมายถึง กลุ่มเครื่องใช้งานประเภทต่างๆ (resources group) โดยมีรายละเอียดดังนี้
<!-- markdownlint-enable MD013 -->
|   Partition   |              รายละเอียด           |
|:-------------:|:--------------------------------|
|`compute`      | กลุ่มเครื่อง compute job            |
|`memory`       | กลุ่มเครื่อง high memory job        |
|`memory-preemt`| กลุ่มเครื่อง Preemt high memory job |
|`gpu`          | กลุ่มเครื่อง GPU Job                |
|`interactive`  | กลุ่มเครื่อง Interactive job        |
<!-- markdownlint-enable MD013 -->

&nbsp;

สถานะของเครื่อง (Node State)
---------------------------
#### รายละเอียดของสถานะเครื่อง (Node state)
<!-- markdownlint-enable MD013 -->
|   State   |                                       รายละเอียด                                  |
|:---------:|:--------------------------------------------------------------------------------|
|`idile`    | สถานะเครื่องไม่มีการจอง รันงาน และว่างพร้อมให้ใช้งาน                                      |
|`alloc`    | สถานะของเครื่องที่มีการใช้งานหรือรันงานเต็มเครื่อง ไม่สามารถเข้าใช้งานได้                        |
|`mix`      | สถานะของเครื่องที่มีการจองใช้งาน CPU บางส่วน (ไม่ได้จองทั้งหมด) ยังมีบางส่วนที่สามารถเข้าใช้งานได้ |
|`down`     | สถานะเครื่องไม่พร้อมให้ใช้งาน                                                         |
|`drain`    | สถานะเครื่องไม่พร้อมให้ใช้งาน เนื่องจากเกิดปัญหาภายในระบบ                                 |
<!-- markdownlint-enable MD013 -->

&nbsp;

**คำสั่งพื้นฐานในการใช้งาน SLURM (Basic SLURM Command)**
------------------------
- การดูสถานะของ Partition (Partition status)
- การดูสถานะของ Job (Show job status)
- การยกเลิก Job (Cancel job)

&nbsp;

การดูสถานะของ Partition (Partition status)
-------------------------------------------
`$ sinfo`  	 คือ คำสั่งที่ใช้ดูสถานะต่างๆ ของ Partition ดังรายละเอียดด้านล่าง 

```
[tara@tara-frontend-1-node-ib ~]$ sinfo
PARTITION      AVAIL  TIMELIMIT  NODES  STATE NODELIST
interactive       up    2:00:00     10  alloc tara-c-[051-060]
compute*          up 5-00:00:00      4    mix tara-c-[010,037,039,044]
compute*          up 5-00:00:00     56  alloc tara-c-[001-009,011-036,038,040-043,045-060]
memory            up 5-00:00:00      7    mix tara-m-[002-004,006-008,010]
memory            up 5-00:00:00      3  alloc tara-m-[001,005,009]
memory-preempt    up 5-00:00:00      4    mix tara-m-[006-008,010]
memory-preempt    up 5-00:00:00      1  alloc tara-m-009
gpu               up 5-00:00:00      1  alloc tara-g-001
gpu               up 5-00:00:00      1   idle tara-g-002

```
*Tips Partition* ( * )  *Partition ที่มีเครื่องหมาย ( * ) *คือ ถ้าไม่มีการระบุในตอน Submit job ว่าจะใช้ Partition ไหน SLURM จะใส่ Partition ที่ขึ้นเครื่องหมาย ( * )* โดยอัตโนมัติ*

&nbsp;

จากรายละเอียดด้านบน สามารถดูข้อมูลได้ดังนี้
- Partition คือ กลุ่มประเภทเครื่องที่ใช้งาน (Resource group)
- Timelimit คือ เวลาสูงสุดที่สามารถใช้งานได้
- Nodes คือ จำนวนเครื่องที่มีให้ใช้งานตามกลุ่มประเภทเครื่องที่ใช้งาน (Resource group)
- State คือ สถานะของเครื่องต่างๆ 
- Node list คือ ชื่อเครื่อง

### sinfo information: **https://slurm.schedmd.com/sinfo.html**

&nbsp;

### การดูปริมาณใช้งานและคงเหลือของหน่วยให้บริการ (SU)

สามารถดู ปริมาณการใช้งานและปริมาณคงเหลือของหน่วยให้บริการ (SU) โดยใช้คำสั่ง

`$ sbalance`
```
[tara@tara-frontend-1-node-ib ~]$ sbalance
Account balances for user: tara
Account: thaisc
        Description:                     thaisc
        Allocation:            1000000000.00 SU
        Remaining Balance:      999162062.00 SU ( 99.92%)
        Used:                      837938.00 SU
Account: tutorial
        Description:                   tutorial
        Allocation:               1000000.00 SU
        Remaining Balance:         774183.00 SU ( 77.42%)
        Used:                      225817.00 SU
```